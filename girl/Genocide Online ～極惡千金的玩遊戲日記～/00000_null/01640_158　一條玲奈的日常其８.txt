作者的話：
玲奈好可愛─！！

各位！！漢⋯⋯漢尼斯君的！！名字！！要記住哦！！（看著感想欄說道）


════════════════════

「玲〜奈！」
「⋯⋯是舞，嗎」

嚇了我一跳呢，沒想到從宅邸的地方出來後，立即就碰上舞了⋯⋯看起來是在門前等待著的呢？似乎無所事事的結城也在她身後。

「⋯⋯能一起去學校嗎？」
「我不介意哦」

舞以宛如附帶『蹦！』這樣具漫畫風格的擬聲詞的動作移動到我的身後，就這樣一邊抓住我的衣袖從旁凝視著我，一邊提議一起上學，於是我回答同意，然後開始走著。

「⋯⋯那個，玲奈」
「？怎麼了？」

在有著一成不變的風景的上學路上與結城和舞三人一起走著的時候，舞再度向我搭話了。特意來到家門前等著，是有甚麼重要的話要說嗎？

「那個，嗯⋯⋯最近有沒有⋯⋯甚麼煩惱之類的嗎？」
「煩惱，嗎⋯⋯」

我一邊在剛好的位置摸著舞的頭部一邊思考⋯⋯我認為我是和往常一樣的，但也許有甚麼可疑之處嗎？抑或是⋯⋯啊啊，難道是在說之前的雙胞胎的事嗎？舞也暗中掌握得到了嗎。

「嗚嘿，嗚嘿嘿⋯⋯」
「是呢，現在並──沒事嗎？」
「──哈？！」

現在並沒有問題哦，正當我想如此傳達給舞並轉過臉看向她的時候⋯⋯不知為何她以不檢點的表情神遊著，令我不禁縮回了摸著她的手。⋯⋯還很睏嗎？

「沒、沒沒沒、沒事！！甚麼問題也沒有！！」
「⋯⋯是嗎？」

雖然舞在拚命地辯解著，但她間或地多次瞥著我的手呢⋯⋯難道是那麼討厭被摸頭嗎？還是說被摸出了奇怪的電波呢？

「⋯⋯⋯⋯噗噗！」
「⋯⋯你，這，在笑甚麼啊織田！」
「不，因為，這樣的⋯⋯噗噗！」
「啊啊──！！織田你又笑了──！！」

哎呀哎呀，他們又開始一如既往的互相嬉戲了呢？這已經成為不看見這個的話會感覺不到一天已經開始的程度了呢，真的是怎麼看都看不膩。『普通』的吵架應該就像是這樣子的。

「因為舞的表情真不檢點⋯⋯噗噗！」
「才、才沒有那麼奇怪啦！！」
「不啊，才不⋯⋯嗯，沒有呢。不要太經常讓外面的人看見比較好吧？」
「嘰咿─！迎迎只是枝填【明明只是織田】！」

被織田拉著兩邊臉頰，使得舞的表情變得很可悲，聲音也不能好好地發出來⋯⋯雖然舞亦伸出手試圖反擊，但身高完全不夠導致她碰不到織田的臉頰。

「⋯⋯果然還是變得很可憐了，午休的時候請你喝牛奶吧」
「仔富用啊【才不用啊】〜！已企側禾嘿哦沙搜啊【比起這個給我縮手啊】〜！」
「⋯⋯⋯⋯嗯！不知道你在說甚麼呢！」
「嘰咿─！」

身高和臂力不足的話，就會被玩弄到這種程度的呢⋯⋯我的話會怎樣呢？我比女生的平均高度高出不過幾厘米左右，不過如果有井上先生在的話筋力也就沒有問題，所以應該沒事的吧？⋯⋯但是在變態紳士先生的那時候不知為何放棄職務了，所以有點微妙呢。

「『啊』」
「？⋯⋯啊啊，早上好」

我一邊眺望著結城和舞招人微笑的爭執一邊走著的時候，聽見了重疊的男性的聲音⋯⋯回頭一看後發現漢尼斯同學一行人就在那邊。那就向他們打招呼吧。

「哦、哦⋯⋯」
「⋯⋯噗噗！」

呼嗯，是與漢尼斯同學相稱的冷淡的回應呢？雖然我倒是不介意⋯⋯不過對於這個應對，克林同學笑了，於是漢尼斯同學稍微碰了那樣的他一下⋯⋯果然『普通』的朋友總之都是笑了的便會被輕輕戳碰的嗎？

「去吧，有想說的事對吧？」
「⋯⋯」
「？漢尼斯同學，找我有甚麼事嗎？」

不知為何今天有很多人找我有事呢？難道這裡最近發生了甚麼大事嗎？我一邊思考這件事一邊詢問後，被艾蕾諾雅同學催促而露出了不情願的樣子的漢尼斯同學向這邊轉過身來。

「⋯⋯不要在現實用那個名字叫我啊」
「⋯⋯也是呢，正樹同學」

噢，這可不行呢⋯⋯因為比起實際【現實】，玩遊戲的時候更令人高興吧，於是無意中以遊戲中的感覺來接觸他了，我必須小心注意才行。

「嘛算了，稍微過來一下⋯⋯給點面子吧」
「？⋯⋯這樣簡直就像是勒索一樣呢？」
「⋯⋯煩死了」

正樹同學把我帶到稍稍偏離道路的小巷之前，停了下來然後把臉湊了過來⋯⋯在背後，正樹同學的朋友們，以及被結城拉著臉頰、拚命將手伸向那樣的他並固定在了這個狀態的舞他們，不知為何如感到擔憂般地注視著我們。⋯⋯兩人仍然還在那樣子呢。

「你，啊⋯⋯那個⋯⋯那個啊⋯⋯」
「？⋯⋯甚麼事呢？」

不知為何正樹同學在吞吞吐吐呢？是那麼難以啟齒的事嗎？視線驚人地往四處張望彷徨著，形跡很可疑呢。

「那個，啊⋯⋯沒事嗎？」
「⋯⋯是指甚麼呢？」
「就是，之前⋯⋯被襲擊了，對吧？」
「啊啊」

原來如此，大概是在擔心我在『貝爾森斯托赫市』裡不知為何遭到玩家大規模襲擊的事吧？⋯⋯正樹同學擔心我，真是罕見呢。

「我倒是沒事⋯⋯你在擔心我嗎？」
「啥？！不、不是啊！就是，那個⋯⋯因為我要打倒你，所以如果你被其他傢伙殺掉甚麼的⋯⋯對，一定只是這樣的！才不要自戀啊！」
「哈⋯⋯」

看來似乎是我誤會了呢，原來如此⋯⋯是那個嗎？母親相當熱情地說的『競爭對手角色？就是傲嬌哦！就是說不想主角被其他傢伙打倒，一邊為自己辯解一邊幫助主角的人哦！』這個嗎？⋯⋯雖然我不是很明白甚麼是傲嬌，也不知道他在對甚麼進行辯解⋯⋯亦沒有得到他甚麼特別的幫助，但是不知為何感覺很相似。

「真是的⋯⋯被我這樣的關係不怎麼好的男人引誘後，就輕易地跟了過來的這一點也讓我很不爽啊！」
「？正樹同學和我不是關係很好的嗎？明明『玩耍』過很多次了？」
「──」

我歪頭凝視著乘勢喋喋不休地說著的正樹同學的臉，並提出了疑問後⋯⋯不知為何他張開嘴巴固定起來了呢？

「⋯⋯果然我討厭你的事」
「那真遺憾呢，我可是喜歡的哦？」
「吵死—了，笨—蛋！！明天的官方活動，給我洗淨脖子等著呢？！」

哎呀？明天已經是第二次的官方活動了嗎，我沒有注意到呢⋯⋯看來我看漏了官方發來的郵件。

「聽清楚了嗎？絕對不—要逃啊！！」
「嗯，不會逃的哦？再次『玩耍』吧？」

我一邊與正樹同學約定在明天的活動裡『玩耍』，一邊目送他走遠的背影⋯⋯他，真的是讓人高興的人呢。

「哎〜，不行呢這⋯⋯」
「噗噗⋯⋯正樹怎可能會坦率呢？」
「別這樣說啊健人」
「不過，沒被討厭不是很好嗎」
「沒錯，對於正樹來說已經是完美的了」
「⋯⋯你們，給我記住了啊？」

把視線從一個接一個結伴走著的他們身上移開的話──

「──還在這樣子嗎？」
「『啊』」

仍然拉著舞的臉頰的結城，和仍然向這樣的他伸出手的舞就顯得非常注目了。


▼▼▼▼▼▼▼

作者的話：
舞醬的可愛讓我寫起來時覺得很開心呢，好想讓久坂んむり先生進行人物設計啊⋯⋯⋯

然後是第２卷的人物設計第４彈啦！！
這次是漢尼斯一行人裡剩下的隊伍成員哦！！

首先是這傢伙！隊伍裡可靠的暗中出力的人！劍士的萊因！

（圖片００１５８─０１）

哇─！好帥─！看著好強！

接下來是神官的心地善良的徹莉！事實上是秩序勢力裡高排名的人哦！！

（圖片００１５８─０２）

柔和又可愛啊─！！

再接下來是隊伍的色氣擔當的工口⋯⋯艾蕾諾雅！

（圖片００１５８─０３）

雖然在意對高中生來說顯得老的臉，不過真的好可愛啊！！

這樣的『Genocide Online』的第２卷預定將於６月５日發售！請各位務必購買！！

關於特典ＳＳ，亦是跟上次相同，
・安利美特、虎穴、蜜瓜各自分別有限定特典

（由於部分店舖是沒附帶特典的，有機會在購買第２卷的時候並不附有特典，因此請向店舖本身進行確認）

・部分一般書店用的特典

（特典的有無，請確認書店發放的信息。有特典的各書店都會將「附有特典紙張（特典ペーパー付）」之類的資訊張貼於舖面或發送到推特上的）

・電子書籍用的的限定特典
準備了上述的５種，請各位多多支持。Amazon亦早已公開第２卷的封面圖了哦。真的是非常精彩的插圖。